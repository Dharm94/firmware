#!/usr/bin/python3


import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(24,GPIO.OUT)
#GPIO.setup(12,GPIO.OUT)

while True:
	GPIO.output(24,GPIO.HIGH)
	#GPIO.output(12,GPIO.HIGH)
	time.sleep(1)
	GPIO.output(24,GPIO.LOW)
	#GPIO.output(12,GPIO.LOW)
	time.sleep(1)
